

// у попередньому рішенні зробив перебором, тому що внизу під завданням давалась література "перебираючі методи". подумав, що потрібно робити перебором

function filterBy (arr, typeArr) {
    return arr.filter(elem => typeof elem !== typeArr);
}

console.log(filterBy(['1', 2, 'Hello', null, '21', 4, {age: 27}], 'number'));