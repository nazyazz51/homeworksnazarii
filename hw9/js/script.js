const myList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function list (arr, parent) {
    parent = document.createElement('ul')
    document.body.prepend(parent);
    arr.forEach(function (elem) {
        let myLi = document.createElement('li');
        myLi.innerText = elem;
        parent.append(myLi);
    });
}

list(myList, parent);