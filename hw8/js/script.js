//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = document.getElementsByTagName('p');
for( let i = 0; i < paragraphs.length; i++){
    paragraphs[i].style.backgroundColor = `#ff0000`;
    paragraphs[i].style.opacity = `1`;
}



//Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let elemId = document.getElementById('optionsList');
console.log(elemId);
console.log(elemId.parentNode);
console.log(elemId.childNodes);



//    Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
document.getElementsByClassName('testParagraph'); // такого класу немає, але є id з таким ім*ям.
let contId = document.getElementById('testParagraph');
contId.innerHTML = 'This is a paragraph';




//Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// let elements = document.querySelector('.main-header').children;
let elements = document.querySelectorAll('.main-header > div');


for (let children of elements) {
    console.log(children);
    children.classList.add('nav-item');
}



//    Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

document.querySelectorAll('.section-title').forEach((elem) => {
    elem.classList.remove('section-title');
});




