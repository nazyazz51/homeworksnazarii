/*
Теоретичні питання
1)-----------Опишіть своїми словами, що таке метод об'єкту------------

    Метод об'єкту це функція(-ї), яка працює з цим об'єктом.


2)-----------Який тип даних може мати значення властивості об'єкта?----------

    Воно може мати будь-який тип даних.






3)-----------Об'єкт це посилальний тип даних. Що означає це поняття?----------






Завдання

Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
*
*
*
*
*  */


let createNewUser = function () {
    let firstName = prompt('Your name?');
    let lastName = prompt('Your last name?');
    let newUser = {
        firstName,
        lastName,
        getLogin : function () {
            return `${this.firstName.toLowerCase().slice(0, 1)}` + `${this.lastName.toLowerCase()}`;
        }
    }
      return  newUser;
}
let user = createNewUser();
console.log(user.getLogin());

