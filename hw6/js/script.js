let createNewUser = function () {
    let firstName = prompt('Your name?');
    let lastName = prompt('Your last name?');
    let userAge = prompt('When are you was born? (dd.mm.yyyy)')
    const newUser = {
        firstName,
        lastName,
        birthday: userAge,
        getLogin () {
            return `${this.firstName.toLowerCase().slice(0, 1)}` + `${this.lastName.toLowerCase()}`;
        },
        getAge () {
            return `${new Date().getFullYear() - this.birthday.slice(6, 10)}`
        },
        getPassword () {
            return `${this.firstName.toUpperCase().slice(0, 1)}` + `${this.lastName.toLowerCase()}` + `${this.birthday.slice(6, 10)}`;
        },
    }
    return  newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
